<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class UserException extends Exception
{
    public function report()
    {
        Log::error('User Exception: ' . $this->getMessage());
    }

    public function render($request)
    {
        return response()->json(['error' => $this->getMessage()], 400);
    }
}
