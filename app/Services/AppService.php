<?php

namespace App\Services;

use App\Repositories\BaseRepositoryInterface;

abstract class AppService
{
    protected $repository;

    public function __construct(BaseRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
}
