<?php

namespace App\Services;

use App\Exceptions\UserException;
use App\Models\ChatRooms;
use App\Repositories\Chat\ChatMessageRepository;
use App\Repositories\Chat\ChatRoomRepository;
use App\Repositories\Chat\RoomMessageRecipientRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class ChatServices
{
    protected $chatRoomRepo;
    protected $chatMessRepo;
    protected $roomChatRecipientRepo;

    protected $userRepo;

    public function __construct()
    {
        $this->chatMessRepo = app()->make(ChatMessageRepository::class);
        $this->chatRoomRepo = app()->make(ChatRoomRepository::class);
        $this->roomChatRecipientRepo = app()->make(RoomMessageRecipientRepository::class);
        $this->userRepo = app()->make(UserRepository::class);
    }

    public function sendMessage($data)
    {
        try {
            $userId = Auth::user()->id;
            $userReceiveId = $data['user_receive_id'];
            $this->checkIssetUserRecive($userReceiveId);
            $dataSend = [
                'user_id' => $userId,
                'message' => $data['message']
            ];
            $roomChat = $this->getOrCreateRoomChat($userId, $userReceiveId);
            $roomId = $roomChat->id;
            $dataSend['room_id'] = $roomId;
            $this->saveChatMessage($dataSend);
            $dataRecive = [
                'user_id' => $userReceiveId,
                'room_id' => $roomId
            ];
            $userReceiveData = $this->getUserRecive($dataRecive);
            if (!$userReceiveData)
            {
                $this->saveUserRecive($dataRecive);
            }
            return;
        }
        catch (\Exception $exception)
        {
            Log::info('ChatServices@sendMessage: '.$exception->getMessage());
            return [
                'error' => $exception->getMessage()
            ];
        }
    }

    public function getOrCreateRoomChat($userId, $userReceiveId)
    {

        $roomChat = $this->chatMessRepo->checkIssetRoom($userId, $userReceiveId);
        if ($roomChat === null) {
            $roomChat = $this->createRoomChat();
        }
        return $roomChat;
    }

    public function createRoomChat()
    {
        try {
            $dataRoom = [
                'name' => ChatRooms::ROOM_NAME_DEFAULT
            ];
            DB::beginTransaction();
            $chatRoom = $this->chatRoomRepo->store($dataRoom);
            DB::commit();
            return $chatRoom;
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::info('ChatServices@createRoomChat: ' . $exception);
        }
    }

    public function saveChatMessage($dataSend)
    {
        try {
            DB::beginTransaction();
            $dataChatMessage = $this->chatMessRepo->store($dataSend);
            DB::commit();
            return $dataChatMessage;
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::info('ChatServices@saveChatMessage: ' . $exception);
        }
    }

    public function saveUserRecive($dataRecive)
    {
        try {
            DB::beginTransaction();
            $dataChatMessage = $this->roomChatRecipientRepo->store($dataRecive);
            DB::commit();
            return $dataChatMessage;
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::info('ChatServices@saveUserRecive: ' . $exception);
        }
    }

    public function getUserRecive($dataRecive)
    {
        return $this->roomChatRecipientRepo->findByColumns($dataRecive)->first();

    }

    public function checkIssetUserRecive($userId)
    {
        $userReciveData = $this->userRepo->getById($userId);
        if (!$userReciveData)
        {
            throw new ModelNotFoundException('User not found by ID ' . $userId);
        }
    }

}
