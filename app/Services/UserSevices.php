<?php

namespace App\Services;

use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

class UserSevices extends AppService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login($data)
    {
        if (!Auth::attempt($data)) {
            return [
                'canLogin' => false
            ];
        }
        $user = $this->userRepository->whereWithColumn('email', $data['email'])->first();
        if (!$user || !Hash::check($data['password'], $user->password)) {
            return [
                'canLogin' => false
            ];
        }
        $tokenResult = $user->createToken('API_TOKEN')->plainTextToken;

        return [
            'canLogin' => true,
            'token' => $tokenResult
        ];

    }

    public function register($data)
    {
        try {
            DB::beginTransaction();
            $data['password'] = bcrypt($data['password']);
            $user = $this->userRepository->store($data);
            DB::commit();
            return $user;
        }
        catch (Exception $exception)
        {
            DB::rollBack();
            Log::error($exception->getMessage());
            return;
        }

    }

}
