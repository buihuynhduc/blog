<?php

namespace App\Mail;
use Illuminate\Mail\Mailable;
class NoticeRegiserSuccess extends Mailable
{
    public function build()
    {
        return $this->view('welcome');
    }
}
