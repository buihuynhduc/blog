<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomMessageRecipient extends Model
{
    protected $table = 'room_message_recipients';
    protected $fillable = ['user_id', 'room_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function chatRooms()
    {
        return $this->belongsTo(ChatRooms::class, 'room_id', 'id');
    }

}
