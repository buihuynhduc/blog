<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatRooms extends Model
{
    protected $table = 'chat_rooms';
    protected $fillable = ['id', 'name'];

    const ROOM_NAME_DEFAULT = 'Cuộc Trò Chuyện';
    public function chatMessages()
    {
        return $this->hasMany(ChatMessage::class, 'room_id', 'id');
    }

    public function roomMessageRecipients()
    {
        return $this->hasMany(RoomMessageRecipient::class, 'room_id', 'id');
    }

}
