<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'message', 'room_id'];
    protected $table = 'chat_messages';
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function chatRoom()
    {
        return $this->belongsTo(ChatRooms::class, 'room_id', 'id');
    }
}
