<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Mail\NoticeRegiserSuccess;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
class SendRegisterSuccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $notiRegisterSuccess = new NoticeRegiserSuccess();
            Mail::to($this->user->email)->send($notiRegisterSuccess);
        }
        catch (\Exception $exception)
        {
            Log::info($exception->getMessage());
        }

    }
}
