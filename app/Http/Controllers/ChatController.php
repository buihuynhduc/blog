<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ChatServices;
use Illuminate\Support\Facades\Log;

class ChatController extends Controller
{
    protected $chatServices;

    public function __construct(ChatServices $chatServices)
    {
        $this->chatServices = $chatServices;
    }

    public function sendMessage(Request $request)
    {
        try {
            $request = $request->all();
            $result = $this->chatServices->sendMessage($request);
            if (isset($result['error'])){
                return \response()->json([
                    'status_code' => 400,
                    'message' => ' Đã có lỗi xảy ra']);
            }
            return \response()->json([
                'status_code' => 200,
                'message' => 'Gửi tin nhắn thành công']);
        } catch (\Exception $exception) {
            Log::info('ChatController@sendMessage: ' . $exception->getMessage());
            return \response()->json([
                'status_code' => 400,
                'message' => ' Đã có lỗi xảy ra']);
        }

    }
}
