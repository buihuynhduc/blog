<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    public function permission()
    {
        $user = Auth::user()->with(['roles' => function ($query) {
            $query->with(['permissions' => function($query){
                $query->where('slug', 'edits-post');
            }])->select('id', 'slug')->where('slug', 'admin');
        }])->first()->toArray();
    }
}
