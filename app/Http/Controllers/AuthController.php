<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DB;
use App\Services\UserSevices;
use Mockery\Exception;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserSevices $userService)
    {
        $this->userService = $userService;
    }

    public function login(Request $request)
    {
        try {
            $dataRequets = $request->all();
            $checkLogin = $this->userService->login($dataRequets);
            if ($checkLogin['canLogin']) {
                return response()->json([
                    'status_code' => 200,
                    'access_token' => $checkLogin['token'],
                    'token_type' => 'Bearer',
                ]);
            }


        } catch (\Exception $exception) {
            Log::info('AuthController@login' . $exception->getMessage());
        }
        return response()->json([
            'status_code' => 401,
            'message' => 'Unauthorized'
        ]);

    }

    public function logout()
    {
        try {
            $token = Auth::user()->currentAccessToken()->delete();
            if ($token) {
                return response()->json([
                    'status_code' => 200,
                    'message' => 'logout success'
                ]);
            }
        } catch (\Exception $exception) {
            Log::info('AuthController@login' . $exception->getMessage());
        }
        return response()->json([
            'status_code' => 400,
            'message' => 'Unauthorized'
        ]);

    }

    public function register(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status_code' => 400,
                    'message' => $validator->errors()
                ]);
            }
            $user = $this->userService->register($data);
            if ($user) {
                $success['token'] = $user->createToken('authToken')->plainTextToken;
                return response()->json([
                    'status_code' => 200,
                    'data' => $success,
                    'message' => 'User created successfully.'
                ]);
            }

        } catch (\Exception $exception) {
            Log::info('AuthController@register' . $exception->getMessage());
            return response()->json([
                'status_code' => 400,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function getInfo()
    {
        try {
            $userInfo = Auth::user();
            if ($userInfo) {
                $userInfo = $userInfo->only(['id', 'name', 'email']);
                return response()->json([
                    'status_code' => 200,
                    'data' => $userInfo,
                ]);
            }
        } catch (Exception $exception) {
            Log::info('AuthController@getInfo: ' . $exception->getMessage());
            return response()->json([
                'status_code' => 400,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
