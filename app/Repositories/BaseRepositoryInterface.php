<?php

namespace App\Repositories;

interface BaseRepositoryInterface
{
    public function getModel();

    public function findById($id, array $columns = []);

    public function store(array $data);

    public function update($id, array $data);


    public function with($relations);

    public function delete($id);

    public function deleteIn($columnName, array $array);

    public function whereWithColumn($column, $value);

    public function getFirst();

    public function findByColumns(array $columns);

}
