<?php

namespace App\Repositories\Chat;

use App\Models\RoomMessageRecipient;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class RoomMessageRecipientRepository extends BaseRepository implements RoomMessageRecipientRepositoryInterface
{
    public function __construct(RoomMessageRecipient $model)
    {
        parent::__construct($model);
    }
}
