<?php

namespace App\Repositories\Chat;

use App\Models\ChatMessage;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ChatMessageRepository extends BaseRepository implements ChatMessageRepositoryInterface
{
    public function __construct(ChatMessage $model)
    {
        parent::__construct($model);
    }

    public function checkIssetRoom($userId, $idUserReceive)
    {
        $roomData = $this->model->with([
            'chatRoom' => function ($q) use ($userId, $idUserReceive) {
                $q->whereHas('roomMessageRecipients', function ($query) use ($idUserReceive) {
                    $query->where('user_id', $idUserReceive);
                })->where('id', '!=', null)->first();
            }
        ])
            ->where('user_id', $userId)->first();
        return $roomData->chatRoom ?? null;
    }
}
