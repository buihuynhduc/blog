<?php

namespace App\Repositories\Chat;

use App\Models\ChatRooms;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ChatRoomRepository extends BaseRepository implements ChatRoomRepositoryInterface
{
    public function __construct(ChatRooms $model)
    {
        parent::__construct($model);
    }
}
