<?php

namespace App\Repositories\Chat;

interface ChatMessageRepositoryInterface
{
    public function checkIssetRoom($userId, $idUserReceive);
}
