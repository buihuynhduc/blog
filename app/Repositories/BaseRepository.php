<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;
    protected $originalModel;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function update($id, array $data)
    {
        $model = $this->getById($id);
        $model->update($data);
        return $model;
    }

    public function delete($id)
    {
        $model = $this->getById($id);
        return $model->delete();
    }

    public function findById($id, array $columns = ['*'])
    {
        $result = $this->model->find($id, $columns);
        return $result;
    }

    public function updateOrCreate(array $where, array $data)
    {
        $model = $this->model;
        return $model->updateOrCreate($where, $data);
    }

    public function with($relations)
    {
        $this->model = $this->model->with($relations);
        return $this;
    }

    public function deleteIn($columnName, array $array)
    {
        // TODO: Implement deleteIn() method.
        return $this->model->whereIn($columnName, $array)->delelte();
    }

    public function getModel()
    {
        // TODO: Implement getModel() method.
        return $this->model;
    }

    public function store(array $data)
    {
        // TODO: Implement store() method.
        return $this->model->create($data);
    }

    public function whereWithColumn($column, $value)
    {
        return $this->model->where($column, $value);
    }

    public function getFirst()
    {
        return $this->model->first();
    }

    public function findByColumns(array $columns)
    {
        return $this->model->where($columns);
    }


}
