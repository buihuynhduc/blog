<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/login", [\App\Http\Controllers\AuthController::class, 'login'])->name('login');

Route::middleware('auth:sanctum')->group(function () {
    Route::get("/logout", [\App\Http\Controllers\AuthController::class, 'logout']);
    Route::get("/info", [\App\Http\Controllers\AuthController::class, 'getInfo']);
    Route::post('create-post', [\App\Http\Controllers\PostController::class, 'createPost']);
    Route::get('/roles', [\App\Http\Controllers\PermissionController::class, 'permission']);
    Route::prefix('chat')->group(function () {
        Route::post('send-message', [\App\Http\Controllers\ChatController::class, 'sendMessage']);
    });
});
Route::post("/register", [\App\Http\Controllers\AuthController::class, 'register']);
